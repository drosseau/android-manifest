# androidmanifest

A Clojure library designed to parse and extract information from
AndroidManifest files. This library doesn't do much beyond defining some data
structures and functions to parse a file into them. It is intended to be pulled
into other Android Clojure tooling.
