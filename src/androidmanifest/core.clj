(ns androidmanifest.core
  (:require [clojure.xml :as xml])
  (:require [clojure.java.io :as io])
  (:require [clojure.zip :as zip])
  (:require [clojure.data.zip.xml :as zip-xml]))

; All things manifest!
;
; https://developer.android.com/guide/topics/manifest/manifest-element

(defprotocol ToXMLString
  (to-xml-string [self indent-level]))

(defn- do-indent
  [level]
  (let [inner
        (fn
          [level st]
          (if (<= level 0)
            st
            (recur (- level 1) (str "    " st))))]
    (inner level "")))

(defn- some-not-nil
  [coll]
  (not (nil? (some #(not (nil? %)) coll))))

(defn- zipper-get
  "Helper function to turn zip items into records"
  [map->rec mapper root selector]
  (map #(map->rec (mapper %))
       (zip-xml/xml-> root selector)))

(defn- get-inners-list
  "Helper function to get inner elements from a node"
  [node getter]
  (let [found (getter node)]
    (if (> (count found) 0) found nil)))

(defn- get-attr
  "Helper function to grab an attribute."
  [node selector]
  (zip-xml/attr node selector))

(defn- parse-bool
  [b]
  (let [bool (.toLowerCase b)]
    (= bool "true")))

(defn- get-boolean
  "Get's a boolean from a string value.
  
  If no default is given this defaults to false"
  ([node tag] (get-boolean node tag false))
  ([node tag default]
   (let [attr (zip-xml/attr node tag)]
     (if (not (nil? attr))
       (parse-bool attr)
       default))))

(defn- get-exported
  "Determine if something is exported
  
  This function doesn't only check android:exported. If it finds that, it
  relies on it entirely. Otherwise, it will try to find an intent filter. If
  an intent filter is found then this this is also exported."
  [node]
  (let [ifilts (zip-xml/xml-> node :intent-filter)
        attr (zip-xml/attr node :android:exported)]
    (if (not (nil? attr))
      (parse-bool attr)
      (and (not (nil? ifilts)) (> (count ifilts) 0)))))

; What follows are just record definitions along with functions to retrieve
; these records from the manifest file.

(defrecord uses-permission [name]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str (do-indent indent-level) "<uses-permission android:name=\"" (:name self) "\" />")))

(defn zipper-get-uses-permission
  [man]
  (let [mapper (fn [uses-perm]
                 {:name (get-attr uses-perm :android:name)})]
    (zipper-get map->uses-permission mapper man :uses-permission)))

(defn- map-to-xml-non-nil
  ([e indent-level]
   (if (not (nil? e))
     (str (reduce #(str %1 "\n" %2) (map #(to-xml-string % (+ indent-level 1)) e)) "\n")
     "")))

(defn- fmt-non-nil
  ([v fmt]
   (if (not (nil? v))
     (format fmt v)
     ""))
  ([rec sel fmt]
   (let [v (sel rec)]
     (if (not (nil? v))
       (format fmt v)
       ""))))

(defrecord permission
           [name
            protectionLevel
            icon
            label
            permissionGroup
            description]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<permission android:name=\"%s\"" (:name self))
     (fmt-non-nil protectionLevel " android:protectionLevel=\"%s\"")
     (fmt-non-nil icon " android:icon=\"%s\"")
     (fmt-non-nil label " android:label\"%s\"")
     (fmt-non-nil description " android:description\"%s\"")
     (fmt-non-nil permissionGroup " android:permissionGroup\"%s\"")
     " />")))

(defn- zipper-get-permission
  [man]
  (let [mapper
        (fn
          [perm]
          {:name (get-attr perm :android:name)
           :protectionLevel (get-attr perm :android:protectionLevel)
           :icon (get-attr perm :andriod:icon)
           :label (get-attr perm :android:label)
           :permissionGroup (get-attr perm :permissionGroup)
           :description (get-attr perm :description)})]
    (zipper-get map->permission mapper man :permission)))

(defrecord uses-library
           [name
            required]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<uses-library android:name=\"%s\"" name)
     (fmt-non-nil required " android:required=\"%s\"")
     "/>")))

(defn- zipper-get-uses-library
  [man]
  (let [mapper
        (fn
          [node]
          {:name (get-attr node :android:name)
           :required (get-boolean node :android:required)})]
    (zipper-get map->uses-library mapper man :uses-library)))

(defrecord uses-feature
           [name
            glEsVersion
            required]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     "<uses-feature"
     (fmt-non-nil name " android:name=\"%s\"")
     (fmt-non-nil glEsVersion " android:glEsVersion=\"%s\"")
     (fmt-non-nil required " android:required=\"%s\"")
     " />")))

(defn- zipper-get-uses-feature
  [man]
  (let [mapper
        (fn
          [node]
          {:name (get-attr node :android:name)
           :glEsVersion (get-attr node :android:glEsVersion)
           :required (get-boolean node :android:required)})]
    (zipper-get map->uses-feature mapper man :uses-feature)))

(defrecord data
           [scheme
            host
            port
            path
            pathPattern
            pathPrefix
            mimeType]
  ToXMLString
  (to-xml-string

    [self indent-level]
    (str

     (do-indent indent-level)
     (format "<data")
     (fmt-non-nil scheme " android:scheme=\"%s\"")
     (fmt-non-nil host " android:host=\"%s\"")
     (fmt-non-nil port " android:port=\"%s\"")
     (fmt-non-nil path " android:path=\"%s\"")
     (fmt-non-nil pathPattern " android:pathPattern=\"%s\"")
     (fmt-non-nil pathPrefix " android:pathPrefix=\"%s\"")
     (fmt-non-nil mimeType " android:mimeType=\"%s\"")
     " />")))

(defn- zipper-get-data
  [parent]
  (let [mapper
        (fn
          [node]
          {:scheme (get-attr node :android:scheme)
           :host (get-attr node :android:host)
           :port (get-attr node :android:port)
           :path (get-attr node :android:path)
           :pathPattern (get-attr node :android:pathPattern)
           :pathPrefix (get-attr node :android:pathPrefix)
           :mimeType (get-attr node :android:mimeType)})]
    (zipper-get map->data mapper parent :data)))

(defrecord category
           [name]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<category android:name=\"%s\" />" (:name self)))))

(defn- zipper-get-category
  [parent]
  (let [mapper
        (fn
          [node]
          {:name (get-attr node :android:name)})]
    (zipper-get map->category mapper parent :category)))

(defrecord action
           [name]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<action android:name=\"%s\" />" (:name self)))))

(defn- zipper-get-action
  [parent]
  (let [mapper
        (fn
          [node]
          {:name (get-attr node :android:name)})]
    (zipper-get map->action mapper parent :action)))

(defrecord intent-filter
           [priority
            actions
            categories
            datas]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (if (not (nil? priority))
       (format "<intent-filter android:priority=\"%s\">\n" priority)
       "<intent-filter>\n")
     (map-to-xml-non-nil actions indent-level)
     (map-to-xml-non-nil categories indent-level)
     (map-to-xml-non-nil datas indent-level)
     (do-indent indent-level)
     "</intent-filter>")))

(defn- zipper-get-intent-filter
  [parent]
  (let [mapper
        (fn
          [node]
          {:priority (get-attr node :android:priority)
           :actions (get-inners-list node zipper-get-action)
           :categories (get-inners-list node zipper-get-category)
           :datas (get-inners-list node zipper-get-data)})]
    (zipper-get map->intent-filter mapper parent :intent-filter)))

(defrecord meta-data
           [name
            resource
            value]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<meta-data")
     (fmt-non-nil name " android:name=\"%s\"")
     (fmt-non-nil resource " android:resource=\"%s\"")
     (fmt-non-nil value " android:value=\"%s\"")
     " />")))

(defn- zipper-get-meta-data
  [parent]
  (let [mapper
        (fn
          [node]
          {:name (get-attr node :android:name)
           :resource (get-attr node :android:resource)
           :value (get-attr node :android:value)})]
    (zipper-get map->meta-data mapper parent :meta-data)))

(defrecord grant-uri-permission
           [path
            pathPattern
            pathPrefix]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<grant-uri-permission")
     (fmt-non-nil path " android:path=\"%s\"")
     (fmt-non-nil pathPattern " android:pathPattern=\"%s\"")
     (fmt-non-nil pathPrefix " android:pathPrefix=\"%s\"")
     " />")))

(defn- zipper-get-grant-uri-permission
  [parent]
  (let [mapper
        (fn
          [node]
          {:path (get-attr node :android:path)
           :pathPattern (get-attr node :android:pathPattern)
           :pathPrefix (get-attr node :android:pathPrefix)})]
    (zipper-get map->grant-uri-permission mapper parent :grant-uri-permission)))

(defrecord path-permission
           [path
            pathPrefix
            pathPattern
            permission
            readPermission
            writePermission])

(defn- zipper-get-path-permission
  [parent]
  (let [mapper
        (fn
          [node]
          {:path (get-attr node :android:path)
           :pathPrefix (get-attr node :android:pathPrefix)
           :pathPattern (get-attr node :android:pathPattern)
           :permission (get-attr node :android:permission)
           :readPermission (get-attr node :android:readPermission)
           :writePermission (get-attr node :android:writePermission)})]
    (zipper-get map->path-permission mapper parent :path-permission)))

(defrecord provider
           [authorities
            directBootAware
            enabled
            exported
            grantUriPermissions
            icon
            initOrder
            label
            multiprocess
            name
            permission
            process
            readPermission
            syncable
            writePermission
           ; inner elems
            meta-datas
            grant-uri-permissions
            path-permissions]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (let [has-inner? (some-not-nil [meta-data grant-uri-permissions path-permissions])]
      (str
       (do-indent indent-level)
       (format "<provider")
       (fmt-non-nil authorities " android:authorities=\"%s\"")
       (fmt-non-nil directBootAware " android:directBootAware=\"%s\"")
       (fmt-non-nil enabled " android:enabled=\"%s\"")
       (fmt-non-nil exported " android:exported=\"%s\"")
       (fmt-non-nil grantUriPermissions " android:grantUriPermissions=\"%s\"")
       (fmt-non-nil icon " android:icon=\"%s\"")
       (fmt-non-nil initOrder " android:initOrder=\"%s\"")
       (fmt-non-nil label " android:label=\"%s\"")
       (fmt-non-nil multiprocess " android:multiprocess=\"%s\"")
       (fmt-non-nil name " android:name=\"%s\"")
       (fmt-non-nil permission " android:permission=\"%s\"")
       (fmt-non-nil process " android:process=\"%s\"")
       (fmt-non-nil readPermission " android:readPermission=\"%s\"")
       (fmt-non-nil syncable " android:syncable=\"%s\"")
       (fmt-non-nil writePermission " android:writePermission=\"%s\"")
       (if has-inner?
         (str
          ">\n"
     ; inner elems
          (map-to-xml-non-nil meta-datas indent-level)
          (map-to-xml-non-nil grant-uri-permissions indent-level)
          (map-to-xml-non-nil path-permissions indent-level)
          (do-indent indent-level)
          "</provider>")
         " />")))))

(defn- zipper-get-provider
  [parent]
  (let [mapper
        (fn
          [node]
          {:authorities (get-attr node :android:authorities)
           :directBootAware (get-attr node :android:directBootAware)
           :enabled (get-boolean node :android:enabled true)
           :exported (get-exported node)
           :grantUriPermissions (get-attr node :android:grantUriPermissions)
           :icon (get-attr node :android:icon)
           :initOrder (get-attr node :android:initOrder)
           :label (get-attr node :android:label)
           :multiprocess (get-attr node :android:multiprocess)
           :name (get-attr node :android:name)
           :permission (get-attr node :android:permission)
           :process (get-attr node :android:process)
           :readPermission (get-attr node :android:readPermission)
           :syncable (get-attr node :android:syncable)
           :writePermission (get-attr node :android:writePermission)
           :path-permissions (get-inners-list node zipper-get-path-permission)
           :meta-datas (get-inners-list node zipper-get-meta-data)
           :grant-uri-permission (get-inners-list node zipper-get-grant-uri-permission)})]
    (zipper-get map->provider mapper parent :provider)))

(defrecord layout
           [defaultHeight
            defaultWidth
            gravity
            minHeight
            minWidth]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<layout")
     (fmt-non-nil defaultHeight " android:defaultHeight=\"%s\"")
     (fmt-non-nil defaultWidth " android:defaultWidth=\"%s\"")
     (fmt-non-nil gravity " android:gravity=\"%s\"")
     (fmt-non-nil minHeight " android:minHeight=\"%s\"")
     (fmt-non-nil minWidth " android:minWidth=\"%s\"")
     " />")))

(defn- zipper-get-layout
  [parent]
  (let [mapper
        (fn
          [node]
          {:defaultHeight (get-attr node :android:defaultHeight)
           :defaultWidth (get-attr node :android:defaultWidth)
           :gravity (get-attr node :android:gravity)
           :minHeight (get-attr node :android:minHeight)
           :minWidth (get-attr node :android:minWidth)})]
    (zipper-get map->layout mapper parent :layout)))

(defrecord activity
           [allowEmbedded
            allowTaskReparenting
            alwaysRetainTaskState
            autoRemoveFromRecents
            banner
            clearTaskOnLaunch
            colorMode
            configChanges
            directBootAware
            documentLaunchMode
            enabled
            excludeFromRecents
            exported
            finishOnTaskLaunch
            hardwareAccelerated
            icon
            immersive
            label
            launchMode
            maxRecents
            maxAspectRatio
            multiprocess
            name
            noHistory
            parentActivityName
            persistableMode
            permission
            process
            relinquishTaskIdentity
            resizeableActivity
            screenOrientation
            showForAllUsers
            stateNotNeeded
            supportsPictureInPicture
            taskAffinity
            theme
            uiOptions
            windowSoftInputMode
            ; inner elements
            intent-filters
            meta-datas
            layouts]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (let [has-inner? (some-not-nil [intent-filters meta-datas layouts])]
      (str
       (do-indent indent-level)
       (format "<activity")
       (fmt-non-nil allowEmbedded " android:allowEmbedded=\"%s\"")
       (fmt-non-nil allowTaskReparenting " android:allowTaskReparenting=\"%s\"")
       (fmt-non-nil alwaysRetainTaskState " android:alwaysRetainTaskState=\"%s\"")
       (fmt-non-nil autoRemoveFromRecents " android:autoRemoveFromRecents=\"%s\"")
       (fmt-non-nil banner " android:banner=\"%s\"")
       (fmt-non-nil clearTaskOnLaunch " android:clearTaskOnLaunch=\"%s\"")
       (fmt-non-nil colorMode " android:colorMode=\"%s\"")
       (fmt-non-nil configChanges " android:configChanges=\"%s\"")
       (fmt-non-nil directBootAware " android:directBootAware=\"%s\"")
       (fmt-non-nil documentLaunchMode " android:documentLaunchMode=\"%s\"")
       (fmt-non-nil enabled " android:enabled=\"%s\"")
       (fmt-non-nil excludeFromRecents " android:excludeFromRecents=\"%s\"")
       (fmt-non-nil exported " android:exported=\"%s\"")
       (fmt-non-nil finishOnTaskLaunch " android:finishOnTaskLaunch=\"%s\"")
       (fmt-non-nil hardwareAccelerated " android:hardwareAccelerated=\"%s\"")
       (fmt-non-nil icon " android:icon=\"%s\"")
       (fmt-non-nil immersive " android:immersive=\"%s\"")
       (fmt-non-nil label " android:label=\"%s\"")
       (fmt-non-nil launchMode " android:launchMode=\"%s\"")
       (fmt-non-nil maxRecents " android:maxRecents=\"%s\"")
       (fmt-non-nil maxAspectRatio " android:maxAspectRatio=\"%s\"")
       (fmt-non-nil multiprocess " android:multiprocess=\"%s\"")
       (fmt-non-nil name " android:name=\"%s\"")
       (fmt-non-nil noHistory " android:noHistory=\"%s\"")
       (fmt-non-nil parentActivityName " android:parentActivityName=\"%s\"")
       (fmt-non-nil persistableMode " android:persistableMode=\"%s\"")
       (fmt-non-nil permission " android:permission=\"%s\"")
       (fmt-non-nil process " android:process=\"%s\"")
       (fmt-non-nil relinquishTaskIdentity " android:relinquishTaskIdentity=\"%s\"")
       (fmt-non-nil resizeableActivity " android:resizeableActivity=\"%s\"")
       (fmt-non-nil screenOrientation " android:screenOrientation=\"%s\"")
       (fmt-non-nil showForAllUsers " android:showForAllUsers=\"%s\"")
       (fmt-non-nil stateNotNeeded " android:stateNotNeeded=\"%s\"")
       (fmt-non-nil supportsPictureInPicture " android:supportsPictureInPicture=\"%s\"")
       (fmt-non-nil taskAffinity " android:taskAffinity=\"%s\"")
       (fmt-non-nil theme " android:theme=\"%s\"")
       (fmt-non-nil uiOptions " android:uiOptions=\"%s\"")
       (fmt-non-nil windowSoftInputMode " android:windowSoftInputMode=\"%s\"")
       (if has-inner?
         (str
          ">\n"
     ; inner elements
          (map-to-xml-non-nil intent-filters indent-level)
          (map-to-xml-non-nil meta-datas indent-level)
          (map-to-xml-non-nil layouts indent-level)
          (do-indent indent-level)
          "</activity>")
         " />")))))

(defn- zipper-get-activity
  [parent]
  (let [mapper
        (fn
          [node]
          {:allowEmbedded (get-attr node :android:allowEmbedded)
           :allowTaskReparenting (get-attr node :android:allowTaskReparenting)
           :alwaysRetainTaskState (get-attr node :android:alwaysRetainTaskState)
           :autoRemoveFromRecents (get-attr node :android:autoRemoveFromRecents)
           :banner (get-attr node :android:banner)
           :clearTaskOnLaunch (get-attr node :android:clearTaskOnLaunch)
           :colorMode (get-attr node :android:colorMode)
           :configChanges (get-attr node :android:configChanges)
           :directBootAware (get-attr node :android:directBootAware)
           :documentLaunchMode (get-attr node :android:documentLaunchMode)
           :enabled (get-boolean node :android:enabled true)
           :excludeFromRecents (get-attr node :android:excludeFromRecents)
           :exported (get-exported node)
           :finishOnTaskLaunch (get-attr node :android:finishOnTaskLaunch)
           :hardwareAccelerated (get-attr node :android:hardwareAccelerated)
           :icon (get-attr node :android:icon)
           :immersive (get-attr node :android:immersive)
           :label (get-attr node :android:label)
           :launchMode (get-attr node :android:launchMode)
           :maxRecents (get-attr node :android:maxRecents)
           :maxAspectRatio (get-attr node :android:maxAspectRatio)
           :multiprocess (get-attr node :android:multiprocess)
           :name (get-attr node :android:name)
           :noHistory (get-attr node :android:noHistory)
           :parentActivityName (get-attr node :android:parentActivityName)
           :persistableMode (get-attr node :android:persistableMode)
           :permission (get-attr node :android:permission)
           :process (get-attr node :android:process)
           :relinquishTaskIdentity (get-attr node :android:relinquishTaskIdentity)
           :resizeableActivity (get-attr node :android:resizeableActivity)
           :screenOrientation (get-attr node :android:screenOrientation)
           :showForAllUsers (get-attr node :android:showForAllUsers)
           :stateNotNeeded (get-attr node :android:stateNotNeeded)
           :supportsPictureInPicture (get-attr node :android:supportsPictureInPicture)
           :taskAffinity (get-attr node :android:taskAffinity)
           :theme (get-attr node :android:theme)
           :uiOptions (get-attr node :android:uiOptions)
           :windowSoftInputMode (get-attr node :android:windowSoftInputMode)
            ; inner elements
           :intent-filters (get-inners-list node zipper-get-intent-filter)
           :meta-datas (get-inners-list node zipper-get-meta-data)
           :layouts (get-inners-list node zipper-get-layout)})]
    (zipper-get map->activity mapper parent :activity)))

(defrecord receiver
           [directBootAware
            enabled
            exported
            icon
            label
            name
            permission
            process
            ; inner elems
            intent-filters
            meta-datas]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (let [has-inner? (some-not-nil [intent-filters meta-datas])]
      (str
       (do-indent indent-level)
       (format "<receiver")
       (fmt-non-nil directBootAware " android:directBootAware=\"%s\"")
       (fmt-non-nil enabled " android:enabled=\"%s\"")
       (fmt-non-nil exported " android:exported=\"%s\"")
       (fmt-non-nil icon " android:icon=\"%s\"")
       (fmt-non-nil label " android:label=\"%s\"")
       (fmt-non-nil name " android:name=\"%s\"")
       (fmt-non-nil permission " android:permission=\"%s\"")
       (fmt-non-nil process " android:process=\"%s\"")
       (if has-inner?
         (str
          ">\n"
          (map-to-xml-non-nil intent-filters indent-level)
          (map-to-xml-non-nil meta-datas indent-level)
          (do-indent indent-level)
          "</receiver>")
         " />")))))

(defn- zipper-get-receiver

  [parent]
  (let [mapper
        (fn
          [node]
          {:directBootAware (get-attr node :android:directBootAware)
           :enabled (get-boolean node :android:enabled true)
           :exported (get-exported node)
           :icon (get-attr node :android:icon)
           :label (get-attr node :android:label)
           :name (get-attr node :android:name)
           :permission (get-attr node :android:permission)
           :process (get-attr node :android:process)
            ; inner elems
           :intent-filters (get-inners-list node zipper-get-intent-filter)
           :meta-datas (get-inners-list node zipper-get-meta-data)})]
    (zipper-get map->receiver mapper parent :receiver)))

(defrecord service
           [description
            directBootAware
            enabled
            exported
            icon
            isolatedProcess
            label
            name
            permission
            process
            ; inner elements
            meta-datas
            intent-filters]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (let [has-inner? (some-not-nil [intent-filters meta-datas])]
      (str
       (do-indent indent-level)
       (format "<service")
       (fmt-non-nil description " android:description=\"%s\"")
       (fmt-non-nil directBootAware " android:directBootAware=\"%s\"")
       (fmt-non-nil enabled " android:enabled=\"%s\"")
       (fmt-non-nil exported " android:exported=\"%s\"")
       (fmt-non-nil icon " android:icon=\"%s\"")
       (fmt-non-nil isolatedProcess " android:isolatedProcess=\"%s\"")
       (fmt-non-nil label " android:label=\"%s\"")
       (fmt-non-nil name " android:name=\"%s\"")
       (fmt-non-nil permission " android:permission=\"%s\"")
       (fmt-non-nil process " android:process=\"%s\"")
       (if has-inner?
         (str
          ">\n"
          (map-to-xml-non-nil meta-datas indent-level)
          (map-to-xml-non-nil intent-filters indent-level)
          (do-indent indent-level)
          "</service")
         " />")))))

(defn- zipper-get-service
  [parent]
  (let [mapper
        (fn
          [node]
          {:description (get-attr node :android:description)
           :directBootAware (get-attr node :android:directBootAware)
           :enabled (get-boolean node :android:enabled true)
           :exported (get-exported node)
           :icon (get-attr node :android:icon)
           :isolatedProcess (get-attr node :android:isolatedProcess)
           :label (get-attr node :android:label)
           :name (get-attr node :android:name)
           :permission (get-attr node :android:permission)
           :process (get-attr node :android:process)
            ; inner elements
           :intent-filters (get-inners-list node zipper-get-intent-filter)
           :meta-datas (get-inners-list node zipper-get-meta-data)})]
    (zipper-get map->service mapper parent :service)))

(defrecord activity-alias
           [enabled
            exported
            icon
            label
            name
            permission
            targetActivity
            ; inner elements
            intent-filters
            meta-datas]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<activity-alias")
     (fmt-non-nil enabled " android:enabled=\"%s\"")
     (fmt-non-nil exported " android:exported=\"%s\"")
     (fmt-non-nil icon " android:icon=\"%s\"")
     (fmt-non-nil label " android:label=\"%s\"")
     (fmt-non-nil name " android:name=\"%s\"")
     (fmt-non-nil permission " android:permission=\"%s\"")
     (fmt-non-nil targetActivity " android:targetActivity=\"%s\"")
     ">\n"
     ; inner elements
     (map-to-xml-non-nil intent-filters indent-level)
     (map-to-xml-non-nil meta-datas indent-level)
     (do-indent indent-level)
     "</activity-alias>")))

(defn- zipper-get-activity-alias
  [parent]
  (let [mapper
        (fn
          [node]
          {:enabled (get-boolean node :android:enabled true)
           :exported (get-exported node)
           :icon (get-attr node :android:icon)
           :label (get-attr node :android:label)
           :name (get-attr node :android:name)
           :permission (get-attr node :android:permission)
           :targetActivity  (get-attr node :android:targetActivity)
           :intent-filters (get-inners-list node zipper-get-intent-filter)
           :meta-datas (get-inners-list node zipper-get-meta-data)})]
    (zipper-get map->activity-alias mapper parent :activity-alias)))

(defrecord application
           [allowTaskReparenting
            allowBackup
            allowClearUserData
            backupAgent
            backupInForeground
            banner
            debuggable
            description
            directBootAware
            enabled
            extractNativeLibs
            fullBackupContent
            fullBackupOnly
            hasCode
            hardwareAccelerated
            icon
            isGame
            killAfterRestore
            largeHeap
            label
            logo
            manageSpaceActivity
            name
            networkSecurityConfig
            permission
            persistent
            process
            restoreAnyVersion
            requiredAccountType
            resizeableActivity
            restrictedAccountType
            supportsRtl
            taskAffinity
            testOnly
            theme
            uiOptions
            usesCleartextTraffic
            vmSafeMode
            ; the following are all inner elements
            activities
            activity-aliases
            meta-datas
            services
            receivers
            providers
            uses-libraries]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<application")
     (fmt-non-nil allowTaskReparenting " android:allowTaskReparenting=\"%s\"")
     (fmt-non-nil allowBackup " android:allowBackup=\"%s\"")
     (fmt-non-nil allowClearUserData " android:allowClearUserData=\"%s\"")
     (fmt-non-nil backupAgent " android:backupAgent=\"%s\"")
     (fmt-non-nil backupInForeground " android:backupInForeground=\"%s\"")
     (fmt-non-nil banner " android:banner=\"%s\"")
     (fmt-non-nil debuggable " android:debuggable=\"%s\"")
     (fmt-non-nil description " android:description=\"%s\"")
     (fmt-non-nil directBootAware " android:directBootAware=\"%s\"")
     (fmt-non-nil enabled " android:enabled=\"%s\"")
     (fmt-non-nil extractNativeLibs " android:extractNativeLibs=\"%s\"")
     (fmt-non-nil fullBackupContent " android:fullBackupContent=\"%s\"")
     (fmt-non-nil fullBackupOnly " android:fullBackupOnly=\"%s\"")
     (fmt-non-nil hasCode " android:hasCode=\"%s\"")
     (fmt-non-nil hardwareAccelerated " android:hardwareAccelerated=\"%s\"")
     (fmt-non-nil icon " android:icon=\"%s\"")
     (fmt-non-nil isGame " android:isGame=\"%s\"")
     (fmt-non-nil killAfterRestore " android:killAfterRestore=\"%s\"")
     (fmt-non-nil largeHeap " android:largeHeap=\"%s\"")
     (fmt-non-nil label " android:label=\"%s\"")
     (fmt-non-nil logo " android:logo=\"%s\"")
     (fmt-non-nil manageSpaceActivity " android:manageSpaceActivity=\"%s\"")
     (fmt-non-nil name " android:name=\"%s\"")
     (fmt-non-nil networkSecurityConfig " android:networkSecurityConfig=\"%s\"")
     (fmt-non-nil permission " android:permission=\"%s\"")
     (fmt-non-nil persistent " android:persistent=\"%s\"")
     (fmt-non-nil process " android:process=\"%s\"")
     (fmt-non-nil restoreAnyVersion " android:restoreAnyVersion=\"%s\"")
     (fmt-non-nil requiredAccountType " android:requiredAccountType=\"%s\"")
     (fmt-non-nil resizeableActivity " android:resizeableActivity=\"%s\"")
     (fmt-non-nil restrictedAccountType " android:restrictedAccountType=\"%s\"")
     (fmt-non-nil supportsRtl " android:supportsRtl=\"%s\"")
     (fmt-non-nil taskAffinity " android:taskAffinity=\"%s\"")
     (fmt-non-nil testOnly " android:testOnly=\"%s\"")
     (fmt-non-nil theme " android:theme=\"%s\"")
     (fmt-non-nil uiOptions " android:uiOptions=\"%s\"")
     (fmt-non-nil usesCleartextTraffic " android:usesCleartextTraffic=\"%s\"")
     (fmt-non-nil vmSafeMode " android:vmSafeMode=\"%s\"")
     ">\n"
     ; the following are all inner elements
     (map-to-xml-non-nil uses-libraries indent-level)
     (map-to-xml-non-nil meta-datas indent-level)
     (map-to-xml-non-nil activities indent-level)
     (map-to-xml-non-nil activity-aliases indent-level)
     (map-to-xml-non-nil services indent-level)
     (map-to-xml-non-nil receivers indent-level)
     (map-to-xml-non-nil providers indent-level)
     (do-indent indent-level)
     "</application>")))

(defn- zipper-get-application
  [parent]
  (let [mapper
        (fn
          [node]
          {:allowTaskReparenting (get-attr node :android:allowTaskReparenting)
           :allowBackup (get-attr node :android:allowBackup)
           :allowClearUserData (get-attr node :android:allowClearUserData)
           :backupAgent (get-attr node :android:backupAgent)
           :backupInForeground (get-attr node :android:backupInForeground)
           :banner (get-attr node :android:banner)
           :debuggable (get-attr node :android:debuggable)
           :description (get-attr node :android:description)
           :directBootAware (get-attr node :android:directBootAware)
           :enabled (get-boolean node :android:enabled true)
           :extractNativeLibs (get-attr node :android:extractNativeLibs)
           :fullBackupContent (get-attr node :android:fullBackupContent)
           :fullBackupOnly (get-attr node :android:fullBackupOnly)
           :hasCode (get-attr node :android:hasCode)
           :hardwareAccelerated (get-attr node :android:hardwareAccelerated)
           :icon (get-attr node :android:icon)
           :isGame (get-attr node :android:isGame)
           :killAfterRestore (get-attr node :android:killAfterRestore)
           :largeHeap (get-attr node :android:largeHeap)
           :label (get-attr node :android:label)
           :logo (get-attr node :android:logo)
           :manageSpaceActivity (get-attr node :android:manageSpaceActivity)
           :name (get-attr node :android:name)
           :networkSecurityConfig (get-attr node :android:networkSecurityConfig)
           :permission (get-attr node :android:permission)
           :persistent (get-attr node :android:persistent)
           :process (get-attr node :android:process)
           :restoreAnyVersion (get-attr node :android:restoreAnyVersion)
           :requiredAccountType (get-attr node :android:requiredAccountType)
           :resizeableActivity (get-attr node :android:resizeableActivity)
           :restrictedAccountType (get-attr node :android:restrictedAccountType)
           :supportsRtl (get-attr node :android:supportsRtl)
           :taskAffinity (get-attr node :android:taskAffinity)
           :testOnly (get-attr node :android:testOnly)
           :theme (get-attr node :android:theme)
           :uiOptions (get-attr node :android:uiOptions)
           :usesCleartextTraffic (get-attr node :android:usesCleartextTraffic)
           :vmSafeMode (get-attr node :android:vmSafeMode)
            ; inner elements
           :activities (get-inners-list node zipper-get-activity)
           :activity-aliases (get-inners-list node zipper-get-activity-alias)
           :meta-datas (get-inners-list node zipper-get-meta-data)
           :services (get-inners-list node zipper-get-service)
           :receivers (get-inners-list node zipper-get-receiver)
           :providers (get-inners-list node zipper-get-provider)
           :uses-libraries (get-inners-list node zipper-get-uses-library)})]
    (zipper-get map->application mapper parent :application)))

(defrecord uses-sdk
           [minSdkVersion
            targetSdkVersion
            maxSdkVersion]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<uses-sdk")
     (fmt-non-nil minSdkVersion " android:minSdkVersion=\"%s\"")
     (fmt-non-nil targetSdkVersion " android:targetSdkVersion=\"%s\"")
     (fmt-non-nil maxSdkVersion " android:maxSdkVersion=\"%s\"")
     " />")))

(defn- zipper-get-uses-sdk
  [parent]
  (let [mapper
        (fn
          [node]
          {:minSdkVersion (get-attr node :android:minSdkVersion)
           :targetSdkVersion (get-attr node :android:targetSdkVersion)
           :maxSdkVersion (get-attr node :android:maxSdkVersion)})]
    (zipper-get map->uses-sdk mapper parent :uses-sdk)))

(defrecord permission-group
           [description
            icon
            label
            name]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<permission-group")
     (fmt-non-nil description " android:description=\"%s\"")
     (fmt-non-nil icon " android:icon=\"%s\"")
     (fmt-non-nil label " android:label=\"%s\"")
     (fmt-non-nil name " android:name=\"%s\"")
     " />")))

(defn- zipper-get-permission-group
  [parent]
  (let [mapper
        (fn
          [node]
          {:description (get-attr node :android:description)
           :icon (get-attr node :android:icon)
           :label (get-attr node :android:label)
           :name (get-attr node :android:name)})]
    (zipper-get map->permission-group mapper parent :permission-group)))

(defrecord instrumentation
           [functionalTest
            handleProfiling
            icon
            label
            name
            targetPackage
            targetProcesses]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<instrumentation")
     (fmt-non-nil functionalTest " android:functionalTest=\"%s\"")
     (fmt-non-nil handleProfiling " android:handleProfiling=\"%s\"")
     (fmt-non-nil icon " android:icon=\"%s\"")
     (fmt-non-nil label " android:label=\"%s\"")
     (fmt-non-nil name " android:name=\"%s\"")
     (fmt-non-nil targetPackage " android:targetPackage=\"%s\"")
     (fmt-non-nil targetProcesses " android:targetProcesses=\"%s\"")
     " />")))

(defn- zipper-get-instrumentation
  [parent]
  (let [mapper
        (fn
          [node]
          {:functionalTest (get-attr node :android:functionalTest)
           :handleProfiling (get-attr node :android:handleProfiling)
           :icon (get-attr node :android:icon)
           :label (get-attr node :android:label)
           :name (get-attr node :android:name)
           :targetPackage (get-attr node :android:targetPackage)
           :targetProcesses (get-attr node :android:targetProcesses)})]
    (zipper-get map->instrumentation mapper parent :instrumentation)))

(defrecord permission-tree
           [name
            icon
            label]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<permission-tree")
     (fmt-non-nil name " android:name=\"%s\"")
     (fmt-non-nil icon " android:icon=\"%s\"")
     (fmt-non-nil label " android:label=\"%s\"")
     " />")))

(defn- zipper-get-permission-tree
  [parent]
  (let [mapper
        (fn
          [node]
          {:name (get-attr node :android:name)
           :icon (get-attr node :android:icon)
           :label (get-attr node :android:label)})]
    (zipper-get map->permission-tree mapper parent :permission-tree)))

(defrecord screen
           [screenSize
            screenDensity]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<screen")
     (fmt-non-nil screenSize " android:screenSize=\"%s\"")
     (fmt-non-nil screenDensity " android:screenDensity=\"%s\"")
     " />")))

(defn- zipper-get-screen
  [parent]
  (let [mapper
        (fn
          [node]
          {:screenSize (get-attr node :android:screenSize)
           :screenDensity (get-attr node :android:screenDensity)})]
    (zipper-get map->screen mapper parent :screen)))

(defrecord compatible-screens
           [screens])

(defn- zipper-get-compatible-screens
  [parent]
  (let [mapper
        (fn
          [node]
          {:screens (get-attr node :android:screens)})]
    (zipper-get map->compatible-screens mapper parent :compatible-screens)))

(defrecord supports-screens
           [resizeable
            smallScreen
            normalScreens
            largeScreens
            xlargeScreens
            anyDensity
            requireSmallestWidthDp
            compatibleWidthLimitDp
            largestWidthLimitDp]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<supports-screens")
     (fmt-non-nil resizeable " android:resizeable=\"%s\"")
     (fmt-non-nil smallScreen " android:smallScreen=\"%s\"")
     (fmt-non-nil normalScreens " android:normalScreens=\"%s\"")
     (fmt-non-nil largeScreens " android:largeScreens=\"%s\"")
     (fmt-non-nil xlargeScreens " android:xlargeScreens=\"%s\"")
     (fmt-non-nil anyDensity " android:anyDensity=\"%s\"")
     (fmt-non-nil requireSmallestWidthDp " android:requireSmallestWidthDp=\"%s\"")
     (fmt-non-nil compatibleWidthLimitDp " android:compatibleWidthLimitDp=\"%s\"")
     (fmt-non-nil largestWidthLimitDp " android:largestWidthLimitDp=\"%s\"")
     " />")))

(defn- zipper-get-supports-screens
  [parent]
  (let [mapper
        (fn
          [node]
          {:resizeable (get-attr node :android:resizeable)
           :smallScreen (get-attr node :android:smallScreen)
           :normalScreens (get-attr node :android:normalScreens)
           :largeScreens (get-attr node :android:largeScreens)
           :xlargeScreens (get-attr node :android:xlargeScreens)
           :anyDensity (get-attr node :android:anyDensity)
           :requireSmallestWidthDp (get-attr node :android:requireSmallestWidthDp)
           :compatibleWidthLimitDp (get-attr node :android:compatibleWidthLimitDp)
           :largestWidthLimitDp (get-attr node :android:largestWidthLimitDp)})]
    (zipper-get map->supports-screens mapper parent :supports-screens)))

(defrecord uses-configuration
           [reqFiveWayNav
            reqHardKeyboard
            reqKeyboardType
            reqNavigation
            reqTouchScreen]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<uses-configuration")
     (fmt-non-nil reqFiveWayNav " android:reqFiveWayNav=\"%s\"")
     (fmt-non-nil reqHardKeyboard " android:reqHardKeyboard=\"%s\"")
     (fmt-non-nil reqKeyboardType " android:reqKeyboardType=\"%s\"")
     (fmt-non-nil reqNavigation " android:reqNavigation=\"%s\"")
     (fmt-non-nil reqTouchScreen " android:reqTouchScreen=\"%s\"")
     " />")))

(defn- zipper-get-uses-configuration
  [parent]
  (let [mapper
        (fn
          [node]
          {:reqFiveWayNav (get-attr node :android:reqFiveWayNav)
           :reqHardKeyboard (get-attr node :android:reqHardKeyboard)
           :reqKeyboardType (get-attr node :android:reqKeyboardType)
           :reqNavigation (get-attr node :android:reqNavigation)
           :reqTouchScreen (get-attr node :android:reqTouchScreen)})]
    (zipper-get map->uses-configuration mapper parent :uses-configuration)))

(defrecord supports-gl-texture
           [name]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<supports-gl-texture")
     (fmt-non-nil name " android:name=\"%s\"")
     " />")))

(defn- zipper-get-supports-gl-texture
  [parent]
  (let [mapper
        (fn
          [node]
          {:name (get-attr node :android:name)})]
    (zipper-get map->supports-gl-texture mapper parent :supports-gl-texture)))

(defrecord manifest
           [package
            sharedUserId
            sharedUserLabel
            versionCode
            versionName
            installLocation
            ; inner elements
            uses-sdk
            supports-screens
            supports-gl-textures
            uses-configuration
            permissions
            uses-permissions
            uses-features
            permission-groups
            permission-trees
            application]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     (do-indent indent-level)
     (format "<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\"")
     (fmt-non-nil package " android:package=\"%s\"")
     (fmt-non-nil sharedUserId " android:sharedUserId=\"%s\"")
     (fmt-non-nil sharedUserLabel " android:sharedUserLabel=\"%s\"")
     (fmt-non-nil versionCode " android:versionCode=\"%s\"")
     (fmt-non-nil versionName " android:versionName=\"%s\"")
     (fmt-non-nil installLocation " android:installLocation=\"%s\"")
     ">\n"
     ; inner elements
     (if (not (nil? uses-sdk)) (to-xml-string uses-sdk (+ indent-level 1)) "")
     (map-to-xml-non-nil supports-screens indent-level)
     (map-to-xml-non-nil supports-gl-textures indent-level)
     (map-to-xml-non-nil uses-configuration indent-level)
     (map-to-xml-non-nil permissions indent-level)
     (map-to-xml-non-nil uses-permissions indent-level)
     (map-to-xml-non-nil uses-features indent-level)
     (map-to-xml-non-nil permission-groups indent-level)
     (map-to-xml-non-nil permission-trees indent-level)
     (to-xml-string application (+ indent-level 1))
     "\n"
     (do-indent indent-level)
     "</manifest>")))

(defn- zipper-get-manifest
  [parent]
  (let [mapper
        (fn
          [node]
          {:package (get-attr node :package)
           :sharedUserId (get-attr node :android:sharedUserId)
           :sharedUserLabel (get-attr node :android:sharedUserLabel)
           :versionCode (get-attr node :android:versionCode)
           :versionName (get-attr node :android:versionName)
           :installLocation (get-attr node :android:installLocation)
           ; inner elements
           :uses-permissions (get-inners-list node zipper-get-uses-permission)
           :uses-sdk (get-inners-list node zipper-get-uses-sdk)
           :supports-screens (get-inners-list node zipper-get-supports-screens)
           :supports-gl-textures (get-inners-list node zipper-get-supports-gl-texture)
           :uses-configuration (get-inners-list node zipper-get-uses-configuration)
           :permissions (get-inners-list node zipper-get-permission)
           :uses-features (get-inners-list node zipper-get-uses-feature)
           :permission-groups (get-inners-list node zipper-get-permission-group)
           :permission-trees (get-inners-list node zipper-get-permission-tree)
           :application (first (zipper-get-application (zip-xml/xml1-> node :application)))})]
    (zipper-get map->manifest mapper parent :manifest)))

(defrecord AndroidManifest
           [file
            package
            ; <manifest> element
            manifest]
  ToXMLString
  (to-xml-string
    [self indent-level]
    (str
     "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?>\n"
     (to-xml-string manifest indent-level))))

(defn read-manifest
  "Open the file of the given name and parse it into an AndroidManifest record"
  [fname]
  (with-open [r (io/input-stream fname)]
    (let [root (-> r xml/parse zip/xml-zip)
          manifest (first (zipper-get-manifest root))]
      (map->AndroidManifest
       {:file fname
        :package (:package manifest)
        :manifest manifest}))))

(defn dump-manifest
  [^AndroidManifest man ^String fname]
  (with-open [w (io/writer fname)]
    (.write w (to-xml-string man 0))))
