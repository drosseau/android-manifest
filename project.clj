(defproject androidmanifest "0.1.0-SNAPSHOT"
  :description "Library for some basic operations on AndroidManifest files"
  :url "https://gitlab.com/drosseau/android-manifest"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.zip "0.1.2"]
                 [org.clojure/data.xml "0.0.8"]])
